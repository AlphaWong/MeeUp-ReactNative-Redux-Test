import React, { Component } from 'react'
import { AppRegistry, ScrollView, View, Alert } from 'react-native'
import { Button, Card } from 'react-native-material-design'
import TextField from 'react-native-md-textinput'

import { connect, applyMiddleware } from 'react-redux'
import thunk from 'redux-thunk'

import {
  setFirstName,
  setLastName,
  setCompanyName,
  setDepartmentName,
  setPositionName,
  setEmail,
  setUser,
  getUser
} from '../actions'

class MyProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      label: [
        'First Name',
        'Last Name',
        'Company',
        'Department',
        'Position',
        'Email'
      ],
      button: [
        'save'
      ]
    }
    this.props.dispatch(getUser())
  }

  textHandler(e, labal) {
    let action;
    switch (labal) {
      case 'First Name':
        action = setFirstName
        break
      case 'Last Name':
        action = setLastName
        break
      case 'Company':
        action = setCompanyName
        break
      case 'Department':
        action = setDepartmentName
        break
      case 'Position':
        action = setPositionName
        break
      case 'Email':
        action = setEmail
        break
    }
    // this.props.dispatch(setFirstName(e.nativeEvent.text))
    this.props.dispatch(action(e.nativeEvent.text))
  }

  buttonHandler(e) {
    const _user = this.props.user.user
    this.props.dispatch(setUser(_user))
  }

  inputVaildation() {
    return false
  }

  render() {
    const {user} = this.props
    // console.log(this.props)
    const userProfile = ([user.user]).map(_user => {
      return (
        <View>
          <Card.Body>
            <TextField label={this.state.label[0]} value={_user.firstName} onChange={e => this.textHandler(e, this.state.label[0])} name="firstName" highlightColor={'#00BCD4'} />
            <TextField label={this.state.label[1]} value={_user.lastName} onChange={e => this.textHandler(e, this.state.label[1])} name="lastName" highlightColor={'#00BCD4'} />
          </Card.Body>
          <Card.Body>
            <TextField label={this.state.label[2]} value={_user.company} onChange={e => this.textHandler(e, this.state.label[2])} name="company" highlightColor={'#00BCD4'} />
            <TextField label={this.state.label[3]} value={_user.department} onChange={e => this.textHandler(e, this.state.label[3])} name="department" highlightColor={'#00BCD4'} />
            <TextField label={this.state.label[4]} value={_user.position} onChange={e => this.textHandler(e, this.state.label[4])} name="position" highlightColor={'#00BCD4'} />
          </Card.Body>
          <Card.Body>
            <TextField label={this.state.label[5]} value={_user.email} onChange={e => this.textHandler(e, this.state.label[5])} name="email" keyboardType="email-address" highlightColor={'#00BCD4'} />
          </Card.Body>
        </View>
      )
    })

    const sumbitButton = ([user.user]).map(_user => {
      return (
        <Button
          disabled={
            _user.firstName.length <= 0 ||
            _user.lastName.length <= 0 ||
            _user.company.length <= 0 ||
            _user.department.length <= 0 ||
            _user.position.length <= 0 ||
            _user.email.length <= 0
          }
          text={this.state.button[0]} onPress={e => this.buttonHandler(e)} />
      )
    })

    return (
      <View>
        <ScrollView>
          <Card>
            {userProfile}
            <Card.Actions>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}>
                {sumbitButton}
              </View>
            </Card.Actions>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const App = connect((store) => {
  return {
    user: store.user
  }
})(MyProfile)

export default App