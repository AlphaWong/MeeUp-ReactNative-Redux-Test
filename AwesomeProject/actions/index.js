import firebaseApp from '../firebase'

const itemsRef = firebaseApp.database().ref()

export function setFirstName(firstName) {
    return {
        type: 'SET_USER_FNAME',
        payload: firstName
    }
}

export function setLastName(lastName) {
    return {
        type: 'SET_USER_LNAME',
        payload: lastName
    }
}

export function setCompanyName(companyName) {
    return {
        type: 'SET_USER_CNAME',
        payload: companyName
    }
}

export function setDepartmentName(departmentName) {
    return {
        type: 'SET_USER_DNAME',
        payload: departmentName
    }
}

export function setPositionName(positionName) {
    return {
        type: 'SET_USER_PNAME',
        payload: positionName
    }
}

export function setEmail(email) {
    return {
        type: 'SET_USER_EMAIL',
        payload: email
    }
}

export function setUser(user) {
    // return async function (dispatch) {
    //     itemsRef.push(user)
    //     const response = await fetch('https://httpbin.org/get')
    //     const responseJson = await response.json()
    //     dispatch(setUser(responseJson))
    // }
    return (dispatch) => {
        itemsRef.push(user)
        dispatch(setUserSuccess())
    }
}

export function setUserSuccess() {
    return {
        type: 'SET_USER_SUCCESS'
    }
}

export function getUser() {
    return (dispatch) => {
        itemsRef.on('value', (snap) => {
            const items=[]
            snap.forEach((child)=>{
                items.push(child.val())
            })
            items.reverse()
            if(items.length>0){
                dispatch(getUserSuccess(items[0]))
            }else{
                dispatch(getUserFail())
            }
        });
    }
}

export function getUserSuccess(json) {
    return {
        type: 'GET_USER_SUCCESS',
        payload: json
    }
}

export function getUserFail(json) {
    return {
        type: 'GET_USER_FAIL'
    }
}