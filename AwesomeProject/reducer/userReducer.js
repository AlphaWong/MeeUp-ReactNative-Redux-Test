const initUserState = {
    user: {
        firstName: String(),
        lastName: String(),
        company: String(),
        department: String(),
        position: String(),
        email: String()
    }
}

export default function reducer(state = initUserState, action) {
    switch (action.type) {
        case 'SET_USER_FNAME':
            return { ...state, user: { ...state.user, firstName: action.payload } }
            break
        case 'SET_USER_LNAME':
            return { ...state, user: { ...state.user, lastName: action.payload } }
            break
        case 'SET_USER_CNAME':
            return { ...state, user: { ...state.user, company: action.payload } }
            break
        case 'SET_USER_DNAME':
            return { ...state, user: { ...state.user, department: action.payload } }
            break
        case 'SET_USER_PNAME':
            return { ...state, user: { ...state.user, position: action.payload } }
            break
        case 'SET_USER_EMAIL':
            return { ...state, user: { ...state.user, email: action.payload } }
            break
        case 'GET_USER_SUCCESS':
            return { ...state, user: action.payload }
            break
        default:
            return state
    }
}