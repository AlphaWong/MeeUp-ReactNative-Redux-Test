import * as firebase from 'firebase'

// Initialize Firebase
const firebaseConfig = {
    apiKey: 'AIzaSyDDMOJWAvja0beTtVVsZCKuNdUds4zMPDY',
    authDomain: 'test-9b493.firebaseapp.com',
    databaseURL: 'https://test-9b493.firebaseio.com',
    storageBucket: 'test-9b493.appspot.com',
};

export default firebase.initializeApp(firebaseConfig)
