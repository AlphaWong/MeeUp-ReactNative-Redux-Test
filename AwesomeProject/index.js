import { createStore } from 'redux'
import { Provider,connect } from 'react-redux'
import { combineForms, Form, Control } from 'react-redux-form'
import React from 'react'

//Stores & Reducers
const initialUser = {
    firstName: '',
    lastName: '',
}

const store = createStore(combineForms({
    user: initialUser,
}))

//
class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <MyForm />
            </Provider>
        );
    }
}

//Forms & Controls
class MyForm extends React.Component {
    handleSubmit(val) {
        // Do anything you want with the form value
        console.log(val);
    }

    render() {
        return (
            <Form model="user" onSubmit={(val) => this.handleSubmit(val)}>
                <label>Your name?</label>
                <Control.text model=".name" />
                <button>Submit!</button>
            </Form>
        );
    }
}

// No need to connect()!